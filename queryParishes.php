<?php
require "DataManager.php";

$dm = new DataManager();

//$parishes = $dm->getParishes();

// If all of lat1, long1, lat2, and long2 are present in the GET vars, we'll
// restrict our search results to what's inside that bounding box.
try {
	if (array_key_exists("lat1", $_GET) and 
		array_key_exists("lat2", $_GET) and
		array_key_exists("long1", $_GET) and
		array_key_exists("long2", $_GET)) {
		$lat1 = floatval($_GET["lat1"]);
		$lat2 = floatval($_GET["lat2"]);
		$long1 = floatval($_GET["long1"]);
		$long2 = floatval($_GET["long2"]);
		$parishes = $dm->getParishes($lat1,$long1,$lat2,$long2);
		echo json_encode($parishes);
	}
} catch (Exception $e) {
	// Probably should emit some kind of error in here
}
?>