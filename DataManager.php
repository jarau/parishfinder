<?php
class DataManager
{
	private $db;
	function __construct() {
		$this->db = new PDO('mysql:host=localhost;dbname=parish_data;charset=utf8', 'DataMan', 'gg)C.8J4eah[[~P');
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	}
	
	
	// Returns all parishes--an array of Parish objects
	public function getParishes($lat1 = -90, $long1 = -180, $lat2 = 90, $long2 = 180) {
		$stmt = $this->db->prepare("SELECT * FROM parishes WHERE latitude > ? AND latitude < ? AND longitude > ? AND longitude < ?");
		$stmt->execute(array($lat1,$lat2,$long1,$long2));
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $rows;
	}
}
?>